from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account
# Register your models here.

admin.site.register(ExpenseCategory)

class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(Receipt)

class ReceiptAdmin(admin.ModelAdmin):
    pass
admin.site.register(Account)

class AccountAdmin(admin.ModelAdmin):
    pass
