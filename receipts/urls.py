from django.urls import path
from .views import receipts_list

urlpatterns = [
    path("", receipts_list, name="home")
]
